FROM papierpain/alpine-php81-nginx:latest

LABEL Maintainer="PapierPain <papierpain4287@outlook.fr>"
LABEL Description="Node Alpine container with php (v8.1) and nginx."

RUN apk update && apk upgrade \
    && apk add nodejs npm
